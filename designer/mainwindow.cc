#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QApplication>
#include <QWindow>

#include <QFile>
#include <QFileInfo>
#include <QUiLoader>

#include <QPushButton>
#include <QTreeWidget>
#include <QTreeWidgetItem>
#include <QTableWidget>
#include <QTableWidgetItem>

#include <QMetaProperty>

#include <QBuffer>
#include <QXmlStreamWriter>

#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>

#include <Inventor/Qt/SoQt.h>
#include <Inventor/Qt/viewers/SoQtExaminerViewer.h>

#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoCube.h>
#include <Inventor/nodes/SoCone.h>
#include <Inventor/nodes/SoSphere.h>
#include <Inventor/nodes/SoCylinder.h>
#include <Inventor/nodes/SoTransform.h>
#include <Inventor/nodes/SoMaterial.h>

// #include <Inventor/manips/SoTrackballManip.h>
#include <Inventor/manips/SoTransformerManip.h>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);


    setGeometry (200, 200, 1200, 800);

    ui->vsplitter->setStretchFactor (0, 4); // nulty prvek, 4 dily
    ui->vsplitter->setStretchFactor (1, 1); // dalsi prvek, 1 dil

    ui->hsplitter->setStretchFactor (0, 1);
    ui->hsplitter->setStretchFactor (1, 4);
    ui->hsplitter->setStretchFactor (2, 1);

    displayObjects (this);

    loadUI ("example.ui");

    displayInventor ();



    /*
    QObject * obj = ui->info;
    while (obj->parent() != nullptr)
        obj = obj->parent();
    displayObjects (obj);
    */

    /*
    for (QWidget * widget  : qApp->topLevelWidgets())
        displayObjects (widget);
    */

    /*
    for (QWindow * window  : qApp->topLevelWindows())
        displayObjects (window);
    */

    /*
    QPushButton * button = new QPushButton (this);
    button->setText ("nadpis");
    QString s = button->text ();

    button->text = "nadpis";
    s = button->text;
    */
}

MainWindow::~MainWindow()
{
    delete ui;
}

/* Open Inventor */

void MainWindow::displayInventor ()
{
   QWidget * widget = new QWidget (this);
   SoQtExaminerViewer * examiner = new SoQtExaminerViewer (widget);

   SoSeparator * root = new SoSeparator;

   simpleScene (root);

   // SoCone * cone = new SoCone;
   // root->addChild(cone);

   examiner->setSceneGraph (root);

   displayInventorBranch (ui->tree->invisibleRootItem (), root);

   ui->tabs->addTab (widget, "Open Inventor");
   ui->tabs->setCurrentWidget (widget);

   // NEZAPOMENOUT SoQt::init ((QWidget*) NULL);

}

void MainWindow::simpleScene (SoSeparator * top)
{
    // cone
    SoSeparator * group1 = new SoSeparator;

    SoTransform * shift1 = new SoTransform;
    shift1->translation.setValue (-2.0, 0.0, 0.0);
    group1->addChild (shift1);

    SoMaterial * redMaterial = new SoMaterial;
    redMaterial->diffuseColor.setValue (1.0, 0.0, 0.0);
    group1->addChild (redMaterial);

    // SoTrackballManip * manip = new SoTrackballManip;
    SoTransformerManip * manip = new SoTransformerManip;
    group1->addChild (manip);

    SoCone * cone = new SoCone ();
    group1->addChild (cone);

    top->addChild (group1);

    // sphere
    SoSeparator * group2 = new SoSeparator;

    SoMaterial * greenMaterial = new SoMaterial;
    greenMaterial->diffuseColor.setValue (0.0, 1.0, 0.0);
    group2->addChild (greenMaterial);

    SoSphere * sphere = new SoSphere ();
    group2->addChild (sphere);

    top->addChild (group2);

    // cube
    SoSeparator * group3 = new SoSeparator;

    SoTransform * shift3 = new SoTransform ();
    shift3->translation.setValue (1.6, 0.0, 1.6);
    shift3->scaleFactor.setValue (0.7, 0.7, 0.7);
    group3->addChild (shift3);

    SoMaterial * blueMaterial = new SoMaterial;
    blueMaterial->diffuseColor.setValue (0.0, 0.0, 1.0);
    group3->addChild (blueMaterial);

    SoCube * cube = new SoCube ();
    group3->addChild (cube);

    top->addChild (group3);
}

/* Open Inventor Tree */

void MainWindow::displayInventorBranch (QTreeWidgetItem * target, SoNode * node)
{
    TreeItem * branch = new TreeItem;
    branch->node = node;

    SoType type = node->getTypeId ();
    branch->setText (0, type.getName().getString());

    if (type.isDerivedFrom (SoGroup::getClassTypeId ()) )
    {
       SoGroup * group = dynamic_cast <SoGroup *> (node);
       int count = group->getNumChildren ();
       for (int i = 0; i < count; i++)
       {
           SoNode * sub_node = group->getChild (i);
           displayInventorBranch (branch, sub_node);
       }
    }

    target->addChild (branch);
}

void MainWindow::displayInventorNode (SoNode * node)
{
    current_object = nullptr; // disable property table editing

    ui->prop->clear();
    ui->prop->setColumnCount (2);
    ui->prop->setHorizontalHeaderLabels (QStringList () << "name" << "value");
    ui->prop->setRowCount (0);

   SoFieldContainer * obj = node;

   SoFieldList list;
   obj->getFields (list);
   int len = list.getLength ();


   // display fields
   for (int i = 0; i < len; i++)
   {
       SoField * field = list [i];

       // retrieve field name
       SbName name;
       obj->getFieldName (field, name);

       // retrieve field value
       SbString value;
       field->get (value);

       displayLine (name.getString (), value.getString ());
   }
}

/* UI */

void MainWindow::loadUI (QString fileName)
{
    // #include <QFile> <QFileInfo> <QUiLoader>
    // pridat do .pro QT += uitools
    // Fedora 34 : dnf install qt5-qttools-devel qt5-qttools-static
    // Debian : apt-get install qtbase5-dev-tools qttools5-dev qttools5-dev-tools

    QUiLoader loader;
    QFile file (fileName);
    if (file.open (QFile::ReadOnly))
    {
        QWidget * widget = loader.load (&file, this);
        file.close();

        // widget->show ();

        QFileInfo fi (fileName);
        int inx = ui->tabs->addTab (widget, fi.fileName ());
        ui->tabs->setTabToolTip (inx, fi.absoluteFilePath ());
        ui->tabs->setCurrentIndex (inx);

        displayObjects (widget);
        displayXML (widget);
        displayJSON (widget);
    }
}

/* Tree */

void MainWindow::displayObjects (QObject * obj)
{
    displayBranch (ui->tree->invisibleRootItem (), obj);
}

void MainWindow::displayBranch (QTreeWidgetItem * target, QObject * obj)
{
    const QMetaObject * cls = obj->metaObject();

    QString text = obj->objectName () + " : " + cls->className ();

    TreeItem * node = new TreeItem;
    node->setText (0, text);
    node->obj = obj;
    target->addChild (node);

    for (QObject * item : obj->children())
        displayBranch (node, item);
}

void MainWindow::treeClick (QTreeWidgetItem * node)
{
    TreeItem * item = dynamic_cast < TreeItem * > (node);
    if (item != nullptr)
    {
        if (item->obj != nullptr)
           displayProperties (item->obj);

        if (item->node != nullptr)
           displayInventorNode (item->node);
    }
}

/* PropertyTable */

void MainWindow::displayLine (QString name, QString value)
{
   int line = ui->prop->rowCount ();
   ui->prop->setRowCount (line+1);

   QTableWidgetItem * name_item = new QTableWidgetItem ();
   name_item->setText (name);
   ui->prop->setItem (line, 0, name_item);

   QTableWidgetItem * value_item = new QTableWidgetItem ();
   value_item->setText (value);
   ui->prop->setItem (line, 1, value_item);
}

void MainWindow::displayProperties (QObject * obj)
{
    current_object = nullptr; // disable property table editing

    ui->prop->clear();
    ui->prop->setColumnCount (2);
    ui->prop->setHorizontalHeaderLabels (QStringList () << "name" << "value");
    ui->prop->setRowCount (0);

    const QMetaObject * cls = obj->metaObject( );
    displayLine ("object name", obj->objectName ());
    displayLine ("class name", cls->className ());

    int cnt = cls->propertyCount();
    for (int inx = 0; inx < cnt; inx++)
    {
        // add #include <QMetaProperty>
        QMetaProperty pro = cls->property (inx);
        QVariant value = pro.read (obj);
        displayLine (pro.name(), value.toString());
    }

    current_object = obj; // enable property table edit
}

void MainWindow::on_prop_cellChanged(int row, int column)
{
    QObject * obj = current_object;
    if (column == 1 && obj != nullptr)
    {
        QTableWidgetItem * item = ui->prop->item (row, 0);
        QString name = item->text ();

        item = ui->prop->item (row, 1);
        QString value = item->text ();

        const QMetaObject * cls = obj->metaObject( );
        int inx = cls->indexOfProperty (name.toLatin1());
        if (inx >= 0)
        {
            ui->info->append (name + " = " + value);
            QMetaProperty pro = cls->property (inx);
            QVariant::Type type = pro.type();

            if (type == QVariant::String)
            {
               pro.write (obj, value);
            }
            else if (type == QVariant::Int)
            {
               bool ok;
               int val = value.toInt (&ok) ;
               if (ok)
                  pro.write (obj, val);
            }
        }
    }
}

/* XML */

void MainWindow::displayXML (QObject * obj)
{
    // include QBuffer, QXmlStreamWriter
    QByteArray output; // vysledny text

    QBuffer buffer (&output); // QIODevice
    buffer.open (QIODevice::WriteOnly);

    QXmlStreamWriter writer (&buffer);
    writer.setAutoFormatting (true);
    writer.writeStartDocument ();

    displayXmlObject (writer, obj);

    writer.writeEndDocument ();

    QTextEdit * edit = new QTextEdit (this);
    edit->setText (output);
    // edit->setFont (QFont ("Serif", 24));

    int inx = ui->tabs->addTab (edit, "XML");
    ui->tabs->setCurrentIndex (inx);
}

void MainWindow::displayXmlObject (QXmlStreamWriter & writer, QObject * obj)
{
    const QMetaObject * cls = obj->metaObject( );
    writer.writeStartElement (cls->className ());

    int cnt = cls->propertyCount();
    for (int inx = 0; inx < cnt; inx++)
    {
        // add #include <QMetaProperty>
        QMetaProperty pro = cls->property (inx);
        QVariant value = pro.read (obj);
        writer.writeTextElement (pro.name(), value.toString());
    }


    bool any = false;
    for (QObject * item : obj->children())
    {
        if (!any)
        {
            writer.writeStartElement ("children");
            any = true;
        }
        displayXmlObject (writer, item);
    }
    if (any) writer.writeEndElement ();

    writer.writeEndElement();
}

/* JSON */

void MainWindow::displayJSON (QObject * obj)
{
    // include QJsonObject, QJsonArray, QJsonDocument

    QJsonObject json_obj = displayJsonObject (obj);
    QJsonDocument json_doc = QJsonDocument (json_obj);
    QByteArray output = json_doc.toJson ();

    QTextEdit * edit = new QTextEdit (this);
    edit->setText (output);
    // edit->setFont (QFont ("Serif", 24));

    int inx = ui->tabs->addTab (edit, "JSON");
    ui->tabs->setCurrentIndex (inx);
}

QJsonObject MainWindow::displayJsonObject (QObject * obj)
{

    const QMetaObject * cls = obj->metaObject( );

    QJsonObject result;
    result ["__type"] = cls->className ();

    QJsonObject properties;
    int cnt = cls->propertyCount();
    for (int inx = 0; inx < cnt; inx++)
    {
        // add #include <QMetaProperty>
        QMetaProperty pro = cls->property (inx);
        QVariant value = pro.read (obj);
        properties [pro.name()] = value.toString();
    }
    result["_properties"] = properties;


    bool any = false;
    QJsonArray children;
    for (QObject * item : obj->children())
    {
        if (!any) any = true;
        children.push_back (displayJsonObject (item));
    }
    if (any) result ["children"] = children;

    return result;
}

/* Window */

void MainWindow::on_tree_itemActivated(QTreeWidgetItem * node, int column)
{
    treeClick (node);
}

void MainWindow::on_tree_itemClicked (QTreeWidgetItem *node, int column)
{
    treeClick (node);
}

void MainWindow::on_quitMenu_triggered()
{
    close ();
}

int main (int argc, char *argv[])
{
    QApplication appl (argc, argv);
    SoQt::init ((QWidget*) NULL); /* dulezite */
    MainWindow win;
    win.show();
    return appl.exec();
}

