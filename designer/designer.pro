QT += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

QT += uitools # QUiLoader

# dnf install qt5-qttools-devel qt5-qttools-static

CONFIG += c++11

SOURCES += mainwindow.cc

HEADERS += mainwindow.h

FORMS += mainwindow.ui

INCLUDEPATH += /usr/include/Coin4
LIBS += -lSoQt -lCoin
# dnf install SoQt-devel
