#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <Inventor/nodes/SoSeparator.h>
// INCLUDEPATH += /usr/include/Coin4
// LIBS += -lSoQt -lCoin
// dnf install SoQt-devel

#include <QMainWindow>

#include <QTreeWidgetItem>
#include <QXmlStreamWriter>
#include <QJsonObject>

class TreeItem : public QTreeWidgetItem
{
public:
    QObject * obj = nullptr;
    SoNode * node = nullptr;
};

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void loadUI (QString fileName);
    void displayObjects (QObject * obj); // tree
    void displayProperties (QObject * obj); // property table
    void displayXML (QObject * obj);
    void displayJSON (QObject * obj);


private:
    QObject * current_object = nullptr; // property table object

    void treeClick (QTreeWidgetItem * node);
    void displayBranch (QTreeWidgetItem * target, QObject * obj);
    void displayLine (QString name, QString value);
    void displayXmlObject (QXmlStreamWriter & writer, QObject * obj);
    QJsonObject displayJsonObject (QObject * obj);

    void displayInventor ();
    void simpleScene (SoSeparator * top);

    void displayInventorBranch (QTreeWidgetItem * target, SoNode * node);
    void displayInventorNode (SoNode * node);

private slots:
    void on_quitMenu_triggered();

    void on_tree_itemActivated(QTreeWidgetItem * node, int column);

    void on_tree_itemClicked(QTreeWidgetItem * node, int column);

    void on_prop_cellChanged(int row, int column);

private:
    Ui::MainWindow *ui;
};
#endif // MAINWINDOW_H
