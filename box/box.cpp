#include "box.h"
#include <QPainter>

#include <iostream>
using namespace std;

Box::Box (QWidget *parent)
    : QWidget (parent)
{ }

void Box::paintEvent(QPaintEvent *event)
{
    QPainter painter;
    painter.begin (this);

    int w = width ();
    int h = height ();

    painter.setPen (QColor (255, 0, 0));
    painter.drawLine (0, 0, w-1, 0);

    painter.setPen (QColor (0, 0, 255));
    painter.drawLine (w-1, 0, w-1, h-1);

    painter.setPen (QColor (0, 255, 0));
    painter.drawLine (0, h-1, w-1, h-1);

    painter.setPen (QColor (255, 255, 0));
    painter.drawLine (0, 0, 0, h-1);

    if (press)
    {
        painter.setPen (QColor ("orange"));
        painter.drawLine (0, 0, w-1, h-1);
        painter.drawLine (0, h-1, w-1, 0);
    }

    painter.end ();
}

void Box::mousePressEvent(QMouseEvent *event)
{
    press = true;
    update ();
}

void Box::mouseReleaseEvent(QMouseEvent *event)
{
    press = false;
    update ();
}
