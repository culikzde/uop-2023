#ifndef BOX_H
#define BOX_H

#include <QWidget>

class Box : public QWidget
{
    // Q_OBJECT
public:
    explicit Box (QWidget *parent = nullptr);

private:
    bool press = false;

protected:
    virtual void paintEvent (QPaintEvent * event) override;
    virtual void mousePressEvent(QMouseEvent *event) override;
    virtual void mouseReleaseEvent(QMouseEvent *event) override;
};

#endif // BOX_H
