#ifndef SINGLETON_H
#define SINGLETON_H

class Singleton
{
public:
    // deklarace

private:
    Singleton();

private:
    static Singleton * instance;

public:
    static Singleton * getInstance ()
    {
        if (instance == nullptr)
            instance = new Singleton;
        return instance;
    }
};



#endif // SINGLETON_H
