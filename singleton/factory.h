#ifndef FACTORY_H
#define FACTORY_H

#include <QPushButton>
#include <vector>
using namespace std;

class Factory
{
public:
    Factory ();
    virtual QWidget * createInstance () = 0;
};

class ButtonFactory : public Factory
{
public:
    ButtonFactory () { }
    QWidget * createInstance () { return new QPushButton; };
};

vector <Factory *> factories;

void init ()
{
    factories.push_back (new ButtonFactory);
}

#endif // FACTORY_H
