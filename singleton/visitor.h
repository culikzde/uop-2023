#ifndef VISITOR_H
#define VISITOR_H

#include <QTreeWidget>
#include <QTreeWidgetItem>

class Visitor
{
public:
    Visitor();
    virtual void visit (QTreeWidgetItem * node) = 0;

    void visitTree (QTreeWidget * tree)
    {
        int cnt = tree->topLevelItemCount ();
        for (int inx = 0; inx < cnt; inx++)
        {
            visitNodes (tree->topLevelItem (inx));
        }
    }

    void visitNodes (QTreeWidgetItem * branch)
    {
        visit (branch);
        int cnt = branch->childCount ();
        for (int inx = 0; inx < cnt; inx++)
        {
            visitNodes (branch->child (inx));
        }
    }
};

class SizeVisitor : public Visitor
{
public:
    SizeVisitor();

    int sum;

    void calc (QTreeWidget * tree)
    {
        sum = 0;
        visitTree (tree);
    }

    void visit (QTreeWidgetItem * node)
    {
        sum = sum + node->text (0).length();
    }
};

#endif // VISITOR_H
